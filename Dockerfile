FROM python:3.8-alpine

ENV PYTHONUNBUFFERED 1
WORKDIR /code
COPY . /code/

RUN apk update && \
    apk add --no-cache postgresql-dev python3-dev build-base && \
    pip install --upgrade pip && \
    pip install -r requirements.txt

EXPOSE 5000



CMD /bin/sh -c "/usr/local/bin/python3 manage.py migrate && /usr/local/bin/python3 manage.py runserver 0.0.0.0:5000"