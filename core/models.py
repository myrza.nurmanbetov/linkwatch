from django.db import models
from django.contrib.auth.models import User


class URLClass(models.Model):
    url_link = models.CharField(max_length=255)
    status = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    pause_check = models.BooleanField(default=False)
    check_interval = models.IntegerField(default=60)

    def __str__(self):
        return self.name
