from django.test import TestCase, Client, SimpleTestCase
from .models import URLClass
from django.contrib.auth.models import User
from django.urls import reverse, resolve
from .views import urlListView, check_url_status, SignUpView



class LinkTest(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create()
        self.url = URLClass.objects.create(url_link='www.youtube.com', status=True, user=self.user, name='Youtube', pause_check=False, check_interval=10)

    def test_model_creation(self):
        self.assertEqual(self.url.url_link, "www.youtube.com")
        self.assertEqual(self.url.status, True)
        self.assertEqual(self.url.user, self.user)
        self.assertEqual(self.url.name, "Youtube")
        self.assertEqual(self.url.pause_check, False)
        self.assertEqual(self.url.check_interval, 10)
    
    def test_model_update(self):

        self.url.url_link = "www.reddit.com"
        self.url.status = False
        self.url.name = "Reddit"
        self.url.pause_check = True
        self.url.check_interval = 20
        self.url.save()

        self.assertEqual(self.url.url_link, "www.reddit.com")
        self.assertEqual(self.url.status, False)
        self.assertEqual(self.url.name, "Reddit")
        self.assertEqual(self.url.pause_check, True)
        self.assertEqual(self.url.check_interval, 20)


    def test_model_deletion(self):
        self.url.delete()
        self.assertFalse(URLClass.objects.exists())
    
class ViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse('links')
        self.user = User.objects.create()
        self.model = URLClass.objects.create(url_link='www.youtube.com', status=True, user=self.user, name='Youtube', pause_check=False, check_interval=10)
    
    def test_view_response(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302) #Expected redirect

    

class URLTest(SimpleTestCase):
    def test_url_list_view(self):
        url = reverse('links')
        resolved_view = resolve(url)
        self.assertEqual(resolved_view.func, urlListView)

    