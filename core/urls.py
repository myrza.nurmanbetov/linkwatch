from django.urls import path
from .views import urlListView, interval_checker, status_checker, update_pause_status, Login, SignUpView
from django.contrib.auth.views import  LogoutView



urlpatterns = [
    path('links/', urlListView, name="links"),    
    path('status_check/<int:pk>/', status_checker, name="status_check"),
    path('save-check-interval/<int:pk>/', interval_checker, name='check_interval'),
    path('update-pause-status/<int:pk>/', update_pause_status, name='update_pause_status'),
    path("", Login.as_view(), name="sign_in"),
    path("sign-out/", LogoutView.as_view(), name="sign-out"),
    path("sign_up/", SignUpView.as_view(), name="sign_up"),
    ]
