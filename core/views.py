import json
import requests

from django.shortcuts import render, get_list_or_404
from .models import URLClass
from django.http import JsonResponse, HttpRequest, HttpResponse
from requests.exceptions import RequestException
from urllib3.exceptions import LocationParseError
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.views.generic import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from typing import Any, Dict



@login_required(login_url="/links")
def urlListView(request: HttpRequest) -> HttpResponse:
    url_list = URLClass.objects.raw("SELECT * FROM core_urlclass")
    context: Dict[str, Any] = {"urls":url_list}
    return render(request, "core/links.html", context)


def update_pause_status(request: HttpRequest, pk: int) -> JsonResponse:
    if request.method == 'POST':
        url = URLClass.objects.get(pk=pk)
        if url.pause_check == True:
            url.pause_check = False
        else:
            url.pause_check= True
        url.save()
        return JsonResponse({'success': True})
    return JsonResponse({'success': False})


def status_checker(request: HttpRequest, pk: int) -> JsonResponse:
    link = URLClass.objects.get(pk=pk)
    url = link.url_link
    response = check_url_status(url)
    if response:
        return JsonResponse({"success":True})
    return JsonResponse({ "success":False})

    

def check_url_status(url: str) -> bool or None:
    try:
        response = requests.get(url, allow_redirects=True)
        status_code = response.status_code
        if status_code == 200:
            return True
        else:
            return None
    except LocationParseError:
        return None
    except RequestException:
        return None




def interval_checker(request: HttpRequest, pk: int) -> JsonResponse:    
    if request.method == 'POST':
        url = URLClass.objects.get(id=pk)
        body = json.loads(request.body)
        interval = body.get('check_interval')
        url.check_interval = int(interval)
        url.save()

        data = {'success': True}
        return JsonResponse(data)
    else:
        data = {'success': False}
        return JsonResponse(data)
    

class SignUpView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("sign_in")
    template_name = "core/signup.html"


class Login(LoginView):
    template_name="core/signin.html"
    redirect_authenticated_user = "links"